package restservice.rest.contants;

public class ApiConstants {

    // BASE URL
    public static final String BASE_GITHUB_URL = "https://api.github.com/";

    //Direcciones para visualizarlas en el navegador mas usuarios pasra relizar las respectivas pruebas
    //https://api.github.com/users
    //https://api.github.com/users/jsinix
    //https://api.github.com/users/takeo
    //https://api.github.com/users/jsinix/followers
    //https://api.github.com/users/takeo/followers


    // ENDPOINTS
    public static final String GITHUB_USER_ENDPOINT = "users/{owner}";
    public static final String GITHUB_FOLLOWERS_ENDPOINT = "/users/{owner}/followers";
}
