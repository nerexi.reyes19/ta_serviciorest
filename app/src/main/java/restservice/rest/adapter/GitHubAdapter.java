package restservice.rest.adapter;

import java.util.List;

import restservice.rest.contants.ApiConstants;

import restservice.rest.model.Owner;
import restservice.rest.service.GitHubService;
import retrofit2.Call;

public class GitHubAdapter extends BaseAdapter implements GitHubService {

    private GitHubService gitHubService;

    public GitHubAdapter() {
        super(ApiConstants.BASE_GITHUB_URL);
        gitHubService = createService(GitHubService.class);
    }

    public Call<Owner> getOwner(String owner) {
        return gitHubService.getOwner(owner);
    }

    @Override
    public Call<List<Owner>> getOwnerFollowers(String owner) {
        return gitHubService.getOwnerFollowers(owner);
    }
}
